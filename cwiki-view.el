;; -*- coding: utf-8-mcs-er -*-
(require 'cwiki-format)
(require 'char-db-json)
(require 'concord-turtle-dump)

(defvar chise-wiki-view-url "view.cgi")
(defvar chise-wiki-edit-url "edit.cgi")
(defvar chise-wiki-add-url "add.cgi")

(defun www-edit-display-feature-input-box (char feature-name
						&optional format value)
  (if (symbolp char)
      (setq char (or (concord-decode-object '=id char 'feature)
		     (concord-make-object 'feature char))))
  (unless format
    (setq format 'default))
  (unless value
    (setq value (www-get-feature-value char feature-name)))
  (if (and (symbolp value)
	   (eq format 'wiki-text))
      (setq value (list (list value))))
  (princ
   (format "<p><input type=\"text\" name=\"feature-name\"
size=\"32\" maxlength=\"256\" value=\"%s\">"
	   feature-name))
  (princ (encode-coding-string " \u2190 " 'utf-8-mcs-er))
  (princ
   (format "%s<input type=\"text\" name=\"%s\"
size=\"64\" maxlength=\"256\" value=\"%s\">
<input type=\"submit\" value=\"set\" /></p>
"
	   (if (or (eq format 'HEX)(eq format 'hex))
	       "0x"
	     "")
	   format
	   (mapconcat (lambda (c)
			(cond
                         ;; ((eq c ?<) "&amp;lt;")
                         ;; ((eq c ?>) "&amp;gt;")
			 ((eq c ?\u0022) "&quot;")
			 (t
			  (char-to-string c))))
		      (est-format-list value format nil nil " ")
		      "")))
  )

(defun www-display-object-desc (genre uri-object &optional uri-feature-name
				      image-selection
				      lang level simple
				      uri-feature-name-to-edit editing-format)
  (unless level
    (setq level 0))
  (let ((object (www-uri-decode-object genre uri-object))
	(est-eval-list-feature-items-limit est-eval-list-feature-items-limit)
	(est-view-url-prefix (if uri-feature-name
				 "../.."
			       ".."))
	(rdf-uri-object (if est-hide-cgi-mode
			    (if (string-match "=" uri-object)
				(concat
				 (est-uri-decode-feature-name-body
				  (substring uri-object 0 (match-beginning 0)))
				 ":"
				 (est-uri-decode-feature-name-body
				  (substring uri-object (match-end 0))))
			      uri-object)))
	feature-name-to-display feature-name-to-edit
	base-name-to-edit metadata-name-to-edit
	without-header
	logical-feature chise-wiki-displayed-features
	parents
	GlyphWiki-id HNG-card HNG-card-id HNG-card-cobj ret object-spec
	ucs
	item-code page char-num glyph-image-id
	width height image-cobj base-image x y w h)
    (if (eq level 0)
	(setq level 1
	      without-header nil)
      (setq without-header t))
    (when object
      (when uri-feature-name-to-edit
	(setq feature-name-to-edit
	      (www-uri-decode-feature-name uri-feature-name-to-edit))
	(setq ret (symbol-name feature-name-to-edit))
	(if (string-match "\\*" ret)
	    (setq base-name-to-edit (intern
				     (substring ret 0 (match-beginning 0)))
		  metadata-name-to-edit (intern
					 (substring ret (match-end 0))))
	  (setq base-name-to-edit feature-name-to-edit))
	(when (stringp editing-format)
	  (setq editing-format (intern editing-format))))
      (when (and (eq genre 'character)
		 (= (length uri-object) 1))
	(setq uri-object (www-uri-encode-object object)))
      (when (= level 1)
	(princ
	 (encode-coding-string
	  (format "<head>
<style type=\"text/css\">
<!--
h1 { font-size: 60px; vertical-align: super; }
hr { color: green; }
.ids { vertical-align: middle; font-size: 40px; line-height: 100%%; }
a { text-decoration: none; }
.ids a { color: black; }
ul { margin: 0 0; color: black; }
li { margin: 0 0 0 2em; }
.feature-name { font-family: sans-serif; font-weight: bold; }
.feature-name a { color: black; }

.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    top: 100%%;
    left: 50%%;
    margin-left: -60px;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 5px 0;
    border-radius: 6px;
 
    position: absolute;
    z-index: 1;
}

.value .image-list img { height: 320px; vertical-align: top; }

.list .tooltip:hover .tooltiptext {
    visibility: visible;
}
-->
</style>

<script
 src=\"https://image.chise.org/openseadragon/openseadragon.min.js\"></script>

<title>EsT %s = %s</title>
</head>\n"
		  genre
		  (decode-uri-string uri-object 'utf-8-mcs-er))
	  'utf-8-mcs-er))
	(princ "<body>\n"))
      (when (eq genre 'character)
	(dolist (feature (char-feature-property '$object 'additional-features))
	  (mount-char-attribute-table
	   (char-feature-name-at-domain feature '$rev=latest))))
      (princ
       (if simple
	   (format
	    (if est-hide-cgi-mode
		"<div style=\"text-align:right;\">
<a href=\"../../edit/view.cgi?%s=%s\">
<input type=\"submit\" value=\"Edit\" />
</a>\n"
	      "<div style=\"text-align:right;\">
<a href=\"edit/view.cgi?%s=%s\">
<input type=\"submit\" value=\"Edit\" />
</a>\n")
	    genre rdf-uri-object)
	 (format
	  "<div style=\"text-align:right;\">
<a href=\"../view/%s/%s\">
<input type=\"submit\" value=\"Simple\" />
</a>\n"
	  genre
	  (if (string-match ":" uri-object)
	      (concat
	       (est-uri-encode-feature-name-body
		(substring uri-object 0 (match-beginning 0)))
	       "="
	       (est-uri-encode-feature-name-body
		(substring uri-object (match-end 0))))))))
      (princ
       (format "<input type=\"submit\" value=\"New Account\" />
<a href=\"./%s/index.ttl\"><input type=\"submit\" value=\"RDF (Turtle)\" /></a>
<a href=\"http://www.chise.org/est/rdf.cgi?%s=%s\">
<input type=\"submit\" value=\"XML (old RDF)\" />
</a>%s
</div>
<hr />\n"
	       rdf-uri-object
	       genre rdf-uri-object
	       (if (eq genre 'character)
		   (format "
<a href=\"/est/view/%s/%s/data.json\">
<input type=\"submit\" value=\"JSON\" />
</a>\n%s"
			   genre rdf-uri-object
			   (if (and (or (char-feature object '->HNG@CN/manuscript)
					(char-feature object '->HNG@CN/printed)
					(char-feature object '->HNG@JP/manuscript)
					(char-feature object '->HNG@JP/printed)
					(char-feature object '->HNG@KR)
					(char-feature object '->HNG@MISC))
				    (setq ucs (char-ucs object))
				    (setq ret (decode-char '=ucs ucs))
				    (setq ret
					  (if (setq ucs (get-char-attribute
							 ret '=>ucs*))
					      (decode-char '=ucs ucs)
					    ret)))
			       (format "
<a href=\"https://search.hng-data.org/search/%s\">
<input type=\"submit\" value=\"HNG-data\" />
</a>\n"
				       (www-uri-encode-object ret))
			     ""))
		 "")))
      (when (setq parents (www-get-feature-value object '<-denotational))
	(if (characterp parents)
	    (setq parents (list parents)))
	(princ (format "<p>%s %s</p>\n<hr>\n"
		       (www-format-value-as-char-list parents)
		       (www-format-feature-name '->denotational lang))))
      (when (setq parents (www-get-feature-value object '<-subsumptive))
	(if (characterp parents)
	    (setq parents (list parents)))
	(princ (format "<p>%s %s</p>\n<hr>\n"
		       (www-format-value-as-char-list parents)
		       (www-format-feature-name '->subsumptive lang))))
      (when (eq genre 'character)
	(setq GlyphWiki-id (char-GlyphWiki-id object)))
      (cond
       ((eq genre 'image-resource)
	(princ
	 (if (setq ret (concord-object-get object '=location@iiif))
	     (if (setq base-image
		       (car (concord-object-get object '<-image-segment)))
		 (format "<a href=\"%s...$.zoom-xywh=%d,%d,%d,%d\"
><img alt=\"%s\" src=\"%s\" /></a>"
			 (www-uri-encode-object base-image)
			 (concord-object-get object 'image-offset-x)
			 (concord-object-get object 'image-offset-y)
			 (concord-object-get object 'image-width)
			 (concord-object-get object 'image-height)
			 ret ret)
	       (if (and image-selection
			(string-match "\\([0-9]+\\),\\([0-9]+\\),\\([0-9]+\\),\\([0-9]+\\)" image-selection))
		   (progn
		     (setq x (string-to-int (match-string 1 image-selection))
			   y (string-to-int (match-string 2 image-selection))
			   w (string-to-int (match-string 3 image-selection))
			   h (string-to-int (match-string 4 image-selection)))
		     (setq width (float
				  (concord-object-get object 'image-width)))
                     ;; (setq height (float
                     ;;               (concord-object-get object 'image-height)))
		     (format "<div id=\"openseadragon1\"
 style=\"width: 800px; height: 600px;\"></div>

<script type=\"text/javascript\">
    var viewer;
    viewer = OpenSeadragon({
        id: \"openseadragon1\",
        prefixUrl: \"https://image.chise.org/openseadragon/images/\",
        preserveViewport:   true,
        visibilityRatio:    1,
        minZoomLevel:       1,
        defaultZoomLevel:   1,
        sequenceMode:       true,
        tileSources:   [ \"%s/info.json\" ]
    });
    viewer.addHandler('open', function () {
      var newBounds = new OpenSeadragon.Rect( %f, %f, %f, %f, 0 );
      viewer.viewport.fitBounds(newBounds, true);
      viewer.viewport.applyConstraints();
    });
</script>
"
			   ret
			   (/ x width)
			   (/ y width)
			   (/ w width)
			   (/ h width)
			   ))
		 (format "<div id=\"openseadragon1\"
 style=\"width: 800px; height: 600px;\"></div>

<script type=\"text/javascript\">
    OpenSeadragon({
        id: \"openseadragon1\",
        prefixUrl: \"https://image.chise.org/openseadragon/images/\",
        preserveViewport:   true,
        visibilityRatio:    1,
        minZoomLevel:       1,
        defaultZoomLevel:   1,
        sequenceMode:       true,
        tileSources:   [ \"%s/info.json\" ]
    });
</script>
"
			 ret)))
	   (setq ret (concord-object-get object '=location))
	   (format "<img alt=\"%s\" src=\"%s\" />"
		   ret ret)))
	)
       ((and (eq genre 'character)
	     (not GlyphWiki-id)
	     (setq ret (or (encode-char object '=chise-hdic-tsj)
			   (encode-char object '===chise-hdic-tsj)))
	     (setq ret (decode-char '===chise-hdic-tsj ret))
	     (setq ret (get-char-attribute ret 'hdic-tsj-word-id)))
	(princ
	 (format "<a href=\"https://viewer.hdic.jp/tsj2/%s\"
>%s</a>"
		 ret
		 (www-format-encode-string
		  (est-format-object object 'readable))))
	)
       ((and (eq genre 'character)
	     (not GlyphWiki-id)
	     (setq ret (or (encode-char object '=chise-hdic-syp)
			   (encode-char object '===chise-hdic-syp)))
	     (setq ret (decode-char '===chise-hdic-syp ret))
	     (setq ret (get-char-attribute ret '=hdic-syp-entry-id)))
	(princ
	 (format "<a href=\"https://viewer.hdic.jp/syp/%s\"
>%s</a>"
		 ret
		 (www-format-encode-string
		  (est-format-object object 'readable))))
	)
       ((or (and (eq genre 'glyph-image)
		 (setq image-cobj
		       (car (concord-object-get object '->image-resource))))
	    (and (eq genre 'character)
                 ;; (setq ret (www-format-encode-string
                 ;;            (est-format-object object 'readable)))
                 ;; (string-match
                 ;;  "<img alt=\"SW-JIGUGE\\([45]\\)-\\([0-9][0-9][0-9]\\)\\([0-9][0-9]\\)"
                 ;;  ret)
                 ;; (setq item-code (string-to-int (match-string 1 ret))
                 ;;       page (string-to-int (match-string 2 ret))
                 ;;       char-num (string-to-int (match-string 3 ret)))
		 (cond ((setq ret (or (encode-char object '=shuowen-jiguge)
				      (encode-char object '===shuowen-jiguge5)))
			(setq item-code 5
			      page     (/ ret 100)
			      char-num (% ret 100)))
		       ((setq ret (encode-char object '===shuowen-jiguge4))
			(setq item-code 4
			      page     (/ ret 100)
			      char-num (% ret 100))))
		 (setq glyph-image-id
		       (cond ((eq item-code 4)
			      (intern
			       (format "zinbun/toho/A024/A024%04d/char=%d"
				       (+ page 18) char-num))
			      )
			     ((eq item-code 5)
			      (intern
			       (format "zinbun/toho/A020/A020%04d/char=%d"
				       (+ page 16) char-num))
			      )))
		 (setq image-cobj
		       (concord-decode-object '=id glyph-image-id 'glyph-image))
		 (setq image-cobj
		       (car (concord-object-get image-cobj '->image-resource)))
		 ))
	(if (and (setq ret (concord-object-get image-cobj '=location@iiif))
		 (setq base-image
		       (car (concord-object-get image-cobj '<-image-segment))))
	    (princ
	     (format "<a href=\"../image-resource/%s...$.zoom-xywh=%d,%d,%d,%d\"
><img alt=\"%s\" src=\"%s\" /></a>"
		     (www-uri-encode-object base-image)
		     (concord-object-get image-cobj 'image-offset-x)
		     (concord-object-get image-cobj 'image-offset-y)
		     (concord-object-get image-cobj 'image-width)
		     (concord-object-get image-cobj 'image-height)
		     ret ret))
	  (setq ret (concord-object-get image-cobj '=location))
	  (princ (format "<img alt=\"%s\" src=\"%s\" />"
			 ret ret)))
	)
       ((and (eq genre 'hng-card)
	     (setq ret (concord-object-get object '=hng-card))
	     (setq ret (symbol-name ret))
	     (string-match "\\([0-9]+\\)-\\([0-9]+\\)" ret))
	(setq ret
	      (format
	       "<img alt=\"HNG-card:%s\"
src=\"https://image.hng-data.org/images/HNG/%03d/card/%04d.jpg\"
style=\"height: 480px;\" />"
	       ret
	       (string-to-int (match-string 1 ret))
	       (string-to-int (match-string 2 ret))))
	(princ
	 (if (setq image-cobj
		   (car (concord-object-get object '->image-resource)))
	     (format "<a href=\"../image-resource/%s\">%s</a>"
		     (www-uri-encode-object image-cobj)
		     ret)
	   ret))
	)
       (t
	(setq ret (www-format-encode-string
		   (est-format-object object 'readable)))
	(when (string-match
	       "<img alt=\"HNG\\([0-9]+\\)-\\([0-9]+\\)[a-z]?\"" ret)
	  (setq HNG-card-id
		(intern
		 (format "%d-%d"
			 (string-to-int (match-string 1 ret))
			 (string-to-int (match-string 2 ret)))))
	  (setq HNG-card
		(format "hng-card/rep.id=%s" HNG-card-id)))
	(princ
	 (format "<h%d>%s%s</h%d>\n"
		 level
		 (cond
		  (uri-feature-name
		   (format "<a href=\"%s\">%s</a>"
			   ;; (if est-hide-cgi-mode
			   ;;     "<a href=\"../%s\">%s</a>"
			   ;;   "<a href=\"%s\">%s</a>")
			   (www-uri-make-object-url object uri-object)
			   ret)
		   )
		  (HNG-card
		   (format
		    "<a href=\"../%s\">%s</a>"
		    HNG-card ret)
		   )
		  (t ret))
		 (if GlyphWiki-id
		     (format
		      " <a href=\"https://glyphwiki.org/wiki/%s\"><img alt=\"%s\" src=\"%s/%s.svg\" style=\"vertical-align:middle; width: 60px; height: 60px\"/></a>"
		      GlyphWiki-id
		      GlyphWiki-id
		      chise-wiki-glyphwiki-glyph-image-url
		      GlyphWiki-id)
		   "")
		 level))
	))
      (if (> level 1)
	  (princ "<ul>"))
      (when feature-name-to-edit
	(princ "<form action=\"set.cgi\" method=\"GET\">\n")
	(princ
	 (encode-coding-string
	  (format "<p>(%s : <input type=\"text\" name=\"%s\"
size=\"30\" maxlength=\"30\" value=\"%s\">)</p>
"
		  genre genre
		  (decode-uri-string uri-object 'utf-8-mcs-er))
	  'utf-8-mcs-er)))
      (when (and HNG-card-id
		 (eq genre 'character)
		 (setq HNG-card-cobj
		       (concord-decode-object
			'=id HNG-card-id
			'hng-card)))
	(put-char-attribute object 'sources@HNG/card (list HNG-card-cobj))
	(cond ((setq ret (concord-object-get
			  HNG-card-cobj
			  '->glyph-image@zinbun/takuhon))
	       (put-char-attribute object 'sources@zinbun/takuhon ret)
	       )
	      ((setq ret (concord-object-get
			  HNG-card-cobj
			  '->glyph-image@gallica))
	       (put-char-attribute object 'sources@gallica ret)
	       ))
	)
      (setq object-spec
	    (cond
	     (uri-feature-name
	      (setq feature-name-to-display
		    (www-uri-decode-feature-name uri-feature-name))
	      (mount-char-attribute-table feature-name-to-display)
	      (setq est-eval-list-feature-items-limit nil)
	      (list
	       (cons feature-name-to-display
		     (if (eq genre 'character)
			 (get-char-attribute object feature-name-to-display)
		       (concord-object-get object feature-name-to-display)))))
	     (t
	      (if (eq genre 'character)
		  (char-attribute-alist object)
		(concord-object-spec object)))))
      (when feature-name-to-edit
	(unless (assq base-name-to-edit object-spec)
	  (setq object-spec (cons (cons base-name-to-edit nil)
				  object-spec))))
      (dolist (cell (sort object-spec
			  (lambda (a b)
			    (char-attribute-name<
			     (char-feature-name-sans-versions (car a))
			     (char-feature-name-sans-versions (car b))))))
	(setq logical-feature (char-feature-name-sans-versions (car cell)))
	(unless (memq logical-feature chise-wiki-displayed-features)
	  (push logical-feature chise-wiki-displayed-features)
	  (cond
	   ((and feature-name-to-edit
		 (eq (car cell) feature-name-to-edit))
	    (www-edit-display-feature-input-box
	     object feature-name-to-edit editing-format)
	    )
	   (t
	    (princ
	     (if (= level 1)
		 "<div class=\"feature\" style=\"line-height:150%\">\n"
	       "<li>\n"))
	    (princ
	     (www-format-eval-list
	      (www-feature-format logical-feature)
	      object
	      logical-feature ; (car cell)
	      lang uri-object
	      nil simple))
	    (unless simple
	      (princ
	       (format " <a href=\"%s?%s=%s&feature=%s&format=wiki-text\"
><input type=\"submit\" value=\"note\" /></a>"
		       chise-wiki-edit-url
		       genre
		       (www-format-encode-string uri-object)
		       (www-format-encode-string
			(www-uri-encode-feature-name
			 (intern (format "%s*note"
					 logical-feature ; (car cell)
					 )))))))
	    (when (and feature-name-to-edit
		       (eq base-name-to-edit (car cell)) metadata-name-to-edit)
	      (princ "<ul>\n")
	      (princ "<li>")
	      (www-edit-display-feature-input-box
	       object feature-name-to-edit editing-format)
	      (princ "</li>")
	      (princ "</ul>"))
	    (princ
	     (if (= level 1)
		 "</div>\n"
	       "<li>\n"))
	    ))
	  ))
      (princ
       (if (= level 1)
	   "<p>\n"
	 "<li>\n"))
      (when feature-name-to-edit
	(princ "</form>\n"))
      (unless simple
	(princ
	 (format "<a href=\"%s?%s=%s\"
><input type=\"submit\" value=\"add feature\" /></a>
"
		 chise-wiki-add-url
		 genre
		 (www-format-encode-string uri-object))))
      (princ
       (if (= level 1)
	   "<p>\n"
	 "<li>\n"))
      (when (eq genre 'character)
	(let ((object-str (est-format-object object))
	      encoded-object-for-form)
	  (princ
	   "<form action=\"https://www.chise.org/ids-find\">\n")
	  (princ (www-format-encode-string object-str))
	  (setq encoded-object-for-form
		(with-temp-buffer
		  (insert (encode-coding-string object-str 'utf-8-jp-er))
		  (goto-char (point-min))
		  (while (search-forward "&GT-" nil t)
		    (replace-match "&amp;GT-" t t)
		    )
		  (buffer-string)))
	  (princ
	   (format
	    " <input type=\"text\" name=\"components\"
size=\"30\" maxlength=\"30\" value=\"%s\" />"
	    encoded-object-for-form))
	  (princ
	   (www-format-encode-string
	    "を\u542Bむ\u6F22\u5B57を\u63A2す"))
	  (princ " <input type=\"submit\" value=\"search\" />\n")
	  (princ "</form>\n")

	  (princ
	   "<form action=\"https://www.chise.org/hng-ids-find\">\n")
	  (princ (www-format-encode-string object-str))

	  (princ
	   (format
	    " <input type=\"text\" name=\"components\"
size=\"30\" maxlength=\"30\" value=\"%s\" />"
	    encoded-object-for-form))
	  (princ
	   (www-format-encode-string
	    "を\u542Bむ HNG の\u6F22\u5B57を\u63A2す"))
	  (princ " <input type=\"submit\" value=\"search\" />\n")
	  (princ "</form>\n")
	))
      (princ
       (if (= level 1)
	   "</p>\n"
	 "<li>\n"))
      )))

(defun www-display-feature-desc (uri-feature-name genre uri-object
						  &optional lang simple)
  (let ((rdf-uri-object (if est-hide-cgi-mode
			    (if (and uri-object
				     (string-match "=" uri-object)
				     (concat
				      (est-uri-decode-feature-name-body
				       (substring uri-object 0 (match-beginning 0)))
				      ":"
				      (est-uri-decode-feature-name-body
				       (substring uri-object (match-end 0)))))
				uri-object)))
	(feature-name (www-uri-decode-feature-name uri-feature-name))
	(name@lang (intern (format "name@%s" lang))))
    (princ
     (encode-coding-string
      (format "<head>
<title>EsT feature: %s</title>
</head>\n"
	      feature-name)
      'utf-8-mcs-er))
    (princ "<body>\n")
    (princ
     (if simple
	 (format
	  (if est-hide-cgi-mode
	      "<div style=\"text-align:right;\">
<a href=\"../../../edit/view.cgi?feature=%s&%s=%s\">
<input type=\"submit\" value=\"Edit\" />
</a>
<input type=\"submit\" value=\"New Account\" />
</div>
<hr />\n"
	    "<div style=\"text-align:right;\">
<a href=\"edit/view.cgi?feature=%s&%s=%s\">
<input type=\"submit\" value=\"Edit\" />
</a>
<input type=\"submit\" value=\"New Account\" />
</div>
<hr />\n")
	  uri-feature-name genre rdf-uri-object)
       (format
	"<div style=\"text-align:right;\">
<a href=\"../view/feature/%s&%s/%s\">
<input type=\"submit\" value=\"Simple\" />
</a>
</div>
<hr />\n"
	uri-feature-name genre uri-object)))
    (princ
     (format "<h1>%s</h1>\n"
	     (www-format-encode-string
	      (symbol-name feature-name))))
    (princ (format "<p>name : %s "
		   (or (www-format-feature-name feature-name) "")))
    (unless simple
      (princ
       (format
	" <a href=\"%s?feature=%s&property=name&format=string&%s=%s\">"
	chise-wiki-edit-url
	uri-feature-name
	genre
	uri-object))
      (princ "<input type=\"submit\" value=\"edit\" /></a>\n"))
    (princ "</p>\n")
    (when lang
      (princ "<p>")
      (princ
       (www-format-encode-string
	(format "%s : %s"
		name@lang
		(or (char-feature-property feature-name name@lang) ""))))
      (unless simple
	(princ
	 (format
	  " <a href=\"%s?feature=%s&property=%s&format=string&%s=%s\">"
	  chise-wiki-edit-url
	  uri-feature-name
	  name@lang
	  genre
	  uri-object))
	(princ "<input type=\"submit\" value=\"edit\" /></a>\n"))
      (princ "</p>\n"))
    (www-html-display-paragraph
     (format "type : %s"
             (or (www-feature-type feature-name)
		 ;; (char-feature-property feature-name 'type)
		 'generic)))
    (princ (format "<p>value-format : %s "
		   (www-format-value
		    nil 'value-format 
		    (or (www-feature-value-format feature-name)
			'default)
		    'default
		    'without-tags)
		   ))
    (unless simple
      (princ
       (format
	" <a href=\"%s?feature=%s&property=value-format&format=wiki-text&%s=%s\"
>"
	chise-wiki-edit-url
	uri-feature-name
	genre
	uri-object))
      (princ "<input type=\"submit\" value=\"edit\" /></a>\n"))
    (princ "</p>\n")

    (princ (format "<p>value-presentation-format : %s "
		   (www-format-value
		    nil 'value-presentation-format 
		    (or (www-feature-value-format feature-name)
			'default)
		    'default
		    'without-tags)
		   ))
    (unless simple
      (princ
       (format
	" <a href=\"%s?feature=%s&property=value-presentation-format&format=wiki-text&%s=%s\"
>"
	chise-wiki-edit-url
	uri-feature-name
	genre
	uri-object))
      (princ "<input type=\"submit\" value=\"edit\" /></a>\n"))
    (princ "</p>\n")

    (princ "<p>format : ")
    (www-html-display-text
     (decode-coding-string
      (www-xml-format-list
       (www-feature-format feature-name))
      'utf-8-mcs-er))
    (unless simple
      (princ
       (format
	" <a href=\"%s?feature=%s&property=format&format=wiki-text&%s=%s\"
>"
	chise-wiki-edit-url
	uri-feature-name
	genre
	uri-object))
      (princ "<input type=\"submit\" value=\"edit\" /></a>\n"))
    (princ "</p>\n")
    
    (www-html-display-paragraph
     (format "description : %s"
             (or (decode-coding-string
		  (or (char-feature-property feature-name 'description)
		      "")
		  'utf-8-mcs-er)
		 "")))
    (when lang
      (www-html-display-paragraph
       (format "description@%s : %s"
	       lang
	       (or (char-feature-property
		    feature-name
		    (intern (format "description@%s" lang)))
		   ""))))
    ))
  
(defun www-batch-view ()
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((target (pop command-line-args-left))
	     (user (pop command-line-args-left))
	     (accept-language (pop command-line-args-left))
	     (mode (intern (pop command-line-args-left)))
	     (lang
	      (intern
	       (car (split-string
		     (car (split-string
			   (car (split-string accept-language ","))
			   ";"))
		     "-"))))
	     ret genre)
	(princ "Content-Type: text/html; charset=UTF-8

<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
            \"http://www.w3.org/TR/html4/loose.dtd\">
<html lang=\"ja\">
")
	(cond
	 ((stringp target)
	  (when (string-match "^char=\\(&[^&;]+;\\)" target)
	    (setq ret (match-end 0))
	    (setq target
		  (concat "char="
			  (www-uri-encode-object
			   (www-uri-decode-object
			    'character (match-string 1 target)))
			  (substring target ret))))
	  (setq target
		(mapcar (lambda (cell)
			  (if (string-match "=" cell)
			      (progn
				(setq genre (substring cell 0 (match-beginning 0))
				      ret (substring cell (match-end 0)))
				(cons
				 (intern
				  (decode-uri-string genre 'utf-8-mcs-er))
				 ret))
			    (list (decode-uri-string cell 'utf-8-mcs-er))))
			(split-string target "&")))
	  (setq ret (car target))
	  (cond ((eq (car ret) 'char)
		 (www-display-object-desc
		  'character (cdr ret) (cdr (assq 'feature target))
		  nil
		  lang nil
		  (eq mode 'simple))
		 )
		((eq (car ret) 'feature)
		 (www-display-feature-desc
		  (decode-uri-string (cdr ret) 'utf-8-mcs-er)
		  (car (nth 1 target))
		  (cdr (nth 1 target))
		  lang
		  (eq mode 'simple))
		 )
		(t
		 (www-display-object-desc
		  (car ret) (cdr ret) (cdr (assq 'feature target))
		  nil
		  lang nil
		  (eq mode 'simple))
		 ))
	  ))
	(princ "\n<hr>\n")
	(princ (format "mode=%S\n" mode))
	(princ (format "user=%s\n" user))
        ;; (princ (format "local user=%s\n" (user-login-name)))
	(princ (format "lang=%S\n" lang))
	(princ (encode-coding-string (emacs-version) 'utf-8-jp-er))
        ;; (princ " CHISE ")
        ;; (princ xemacs-chise-version)
	(princ "
</body>
</html>")
	)
    (error nil
	   (princ (format "%S" err)))
    ))

(defun www-batch-view-smart ()
  (setq debug-on-error t)
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((est-hide-cgi-mode t)
	     (target (pop command-line-args-left))
	     (user (pop command-line-args-left))
	     (accept-language (pop command-line-args-left))
	     (mode (intern (pop command-line-args-left)))
	     (lang
	      (intern
	       (car (split-string
		     (car (split-string
			   (car (split-string accept-language ","))
			   ";"))
		     "-"))))
	     ret genre feature obj-url json turtle obj)
	(cond
	 ((stringp target)
	  (cond
	   ((string-match "/data\\.json$" target)
	    (setq json t
		  target (substring target 0 (match-beginning 0)))
	    )
	   ((string-match "/index\\.ttl$" target)
	    (setq turtle t
		  target (substring target 0 (match-beginning 0)))
	    )
	   ((string-match "/index\\.\\(...*\\)\\.html$" target)
	    (setq lang (intern (match-string 1 target))
		  target (substring target 0 (match-beginning 0)))
	    ))
	  (when (string-match "^char/\\(&[^&;]+;\\)" target)
	    (setq ret (match-end 0))
	    (setq target
		  (concat "char/"
			  (www-uri-encode-object
			   (www-uri-decode-object
			    'character (match-string 1 target)))
			  (substring target ret))))
	  (setq target
		(mapcar
		 (lambda (cell)
		   (if (string-match "/" cell)
		       (progn
			 (setq genre (substring cell 0 (match-beginning 0))
			       ret (substring cell (match-end 0)))
			 (cons
			  (intern (decode-uri-string genre 'utf-8-mcs-er))
			  (cond
			   ((string-match "/feature=" ret)
			    (list (substring ret 0 (match-beginning 0))
				  (substring ret (match-end 0)))
			    )
			   ((string-match "...$.zoom-xywh=" ret)
			    (list (substring ret 0 (match-beginning 0))
				  nil
				  (substring ret (match-end 0)))
			    )
			   (t
			    (list ret)))))
		     (list (decode-uri-string cell 'utf-8-mcs-er)))
		   ;; (setq ret (split-string cell "/"))
		   ;; (cons (intern
		   ;;        (decode-uri-string (car ret) 'utf-8-mcs-er))
		   ;;       (cdr ret))
		   )
		 (split-string target "&")))
	  (setq ret (car target))
	  (cond (turtle
		 (princ "Content-Type: text/turtle; charset=UTF-8

")
		 )
		(json
		 (princ "Content-Type: application/json; charset=UTF-8

")
		 )
		(t
		 (princ "Content-Type: text/html; charset=UTF-8

<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
            \"http://www.w3.org/TR/html4/loose.dtd\">
<html lang=\"ja\">
")
		 ))
          ;; (princ (format "<p>%S, %S, %S</p>"
          ;;                (car ret)(nth 1 ret)(nth 2 ret)))
          ;; (princ (format "// %S %S\n" ret json))
	  (cond (turtle
		 (with-temp-buffer
		   (setq obj (www-uri-decode-object (car ret)(nth 1 ret)))
		   (concord-turtle-insert-char-data obj)
		   (goto-char (point-min))
		   (concord-turtle-insert-prefix)
		   (insert "\n")
		   (encode-coding-region
		    (point-min)(point-max)
		    char-db-file-coding-system)
		   (princ (buffer-string)))
		 )
		((or (eq (car ret) 'char)
		     (eq (car ret) 'character))
		 (if (and json
			  (setq obj (www-uri-decode-object
				     (car ret)(nth 1 ret)))
			  (characterp obj))
		     (with-temp-buffer
                       ;; (princ (encode-coding-string
                       ;;         (format "// %S\n" obj)
                       ;;         char-db-file-coding-system))
		       (char-db-json-char-data-with-variant obj 'printable)
		       (encode-coding-region
			(point-min)(point-max)
			char-db-file-coding-system)
		       (princ (buffer-string))
		       )
		   (www-display-object-desc
		    'character (nth 1 ret) (nth 2 ret)
		    nil
		    lang nil
		    (eq mode 'simple)))
		 )
		((eq (car ret) 'feature)
		 (www-display-feature-desc
		  (decode-uri-string (nth 1 ret) 'utf-8-mcs-er)
		  (car (nth 1 target))
		  (nth 1 (nth 1 target))
		  lang
		  (eq mode 'simple))
		 )
                ;; ((eq (car ret) 'image-resource)
                ;; ;;  (cond
                ;; ;;   ((string-match "^\\.iiif=" (nth 1 ret))
                ;; ;;    (setq obj-url (decode-uri-string
                ;; ;;                   (substring (nth 1 ret) (match-end 0))
                ;; ;;                   'utf-8-mcs-er))
                ;; ;;    (setq obj (concord-images-add-iiif obj-url))
                ;; ;;    (www-display-object-desc
                ;; ;;     'image-resource
                ;; ;;     (www-uri-encode-object obj)
                ;; ;;     (nth 2 ret)
                ;; ;;     lang nil
                ;; ;;     (eq mode 'simple))
                ;; ;;    )
                ;; ;;   (t
                ;;  (princ (nth 1 ret))
                ;;  (www-display-object-desc
                ;;   'image-resource (nth 1 ret) (nth 2 ret)
                ;;   lang nil
                ;;   (eq mode 'simple))
                ;; ;;    ))
                ;;  )
		(t
		 (www-display-object-desc
		  (car ret) (nth 1 ret) (nth 2 ret)
		  (nth 3 ret)
		  lang nil
		  (eq mode 'simple))
		 ))
	  ))
	(unless (or json turtle)
	  (princ "\n<hr>\n")
	  (princ (format "mode=%S\n" mode))
	  (princ (format "user=%s\n" user))
	  ;; (princ (format "local user=%s\n" (user-login-name)))
	  (princ (format "lang=%S\n" lang))
	  (princ (encode-coding-string (emacs-version) 'utf-8-jp-er))
	  ;; (princ " CHISE ")
	  ;; (princ xemacs-chise-version)
	  (princ "
</body>
</html>")
	  )
	)
    (error nil
	   (princ (format "%S" err)))
    ))

(provide 'cwiki-view)
