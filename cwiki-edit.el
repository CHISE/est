;; -*- coding: utf-8-mcs-er -*-
(defvar chise-wiki-view-url "view.cgi")
(defvar chise-wiki-edit-url "edit.cgi")

;; (require 'cwiki-format)
(require 'cwiki-view)

;; (defun www-edit-display-input-box (object name value &optional format)
;;   (when (stringp format)
;;     (setq format (intern format)))
;;   (let (prefix)
;;     (if (or (eq format 'HEX)
;;             (eq format 'hex))
;;         (if (integerp value)
;;             (setq prefix "0x")))
;;     (princ (www-format-encode-string
;;             (format "%s \u2190 %s"
;;                     name
;;                     (or prefix ""))))
;;     (princ
;;      (format "<input type=\"text\" name=\"%s\"
;; size=\"30\" maxlength=\"30\" value=\"%s\">
;; <input type=\"submit\" value=\"set\" />
;; 
;; "
;;              (www-format-encode-string
;;               (format "%s" name) 'without-tags)
;;              (www-format-apply-value object name
;;                                      format nil value
;;                                      nil nil
;;                                      'without-tags)
;;              ))))

;; (defun www-edit-display-feature-input-box (char feature-name
;;                                                 &optional format value)
;;   (unless format
;;     (setq format 'default))
;;   (unless value
;;     (setq value (www-get-feature-value char feature-name)))
;;   (princ
;;    (format "<p><input type=\"text\" name=\"feature-name\"
;; size=\"32\" maxlength=\"256\" value=\"%s\">"
;;            feature-name))
;;   (princ (encode-coding-string " \u2190 " 'utf-8-mcs-er))
;;   (princ
;;    (format "%s<input type=\"text\" name=\"%s\"
;; size=\"64\" maxlength=\"256\" value=\"%s\">
;; <input type=\"submit\" value=\"set\" /></p>
;; "
;;            (if (or (eq format 'HEX)(eq format 'hex))
;;                "0x"
;;              "")
;;            format
;;            (mapconcat (lambda (c)
;;                         (cond
;;                          ;; ((eq c ?<) "&amp;lt;")
;;                          ;; ((eq c ?>) "&amp;gt;")
;;                          ((eq c ?\u0022) "&quot;")
;;                          (t
;;                           (char-to-string c))))
;;                       (est-format-unit
;;                        (est-eval-unit
;;                         (if (symbolp format)
;;                             (list format)
;;                           format)
;;                         char feature-name)
;;                        'without-tags)
;;                       ;; (www-format-value char feature-name
;;                       ;;                   value format 'without-tags)
;;                       "")))
;;   )

(defun www-edit-display-object-desc (genre uri-object uri-feature-name
					   &optional lang format)
  (www-display-object-desc genre uri-object nil lang 1 nil
			   uri-feature-name format))

;; (defun www-edit-display-object-desc (genre uri-object uri-feature-name
;;                                            &optional lang format)
;;   (when (stringp format)
;;     (setq format (intern format)))
;;   (let ((object (www-uri-decode-object genre uri-object))
;;         (feature-name (www-uri-decode-feature-name uri-feature-name))
;;         base-name metadata-name
;;         parents
;;         object-spec str)
;;     (when object
;;       (princ
;;        (encode-coding-string
;;         (format "<head>
;; <title>EsT %s = %s</title>
;; </head>\n"
;;                 genre
;;                 (decode-uri-string uri-object 'utf-8-mcs-er))
;;         'utf-8-mcs-er))
;;       (princ "<body>\n")
;;       (when (eq genre 'character)
;;         (dolist (feature (char-feature-property '$object 'additional-features))
;;           (mount-char-attribute-table
;;            (char-feature-name-at-domain feature '$rev=latest))))
;;       (when (setq parents (www-get-feature-value object '<-denotational))
;;         (princ (format "<p>%s %s</p>\n<hr>\n"
;;                        (www-format-value-as-char-list parents)
;;                        (www-format-feature-name '->denotational lang))))
;;       (when (setq parents (www-get-feature-value object '<-subsumptive))
;;         (princ (format "<p>%s %s</p>\n<hr>\n"
;;                        (www-format-value-as-char-list parents)
;;                        (www-format-feature-name '->subsumptive lang))))
;;       (princ
;;        (format "<h1>%s</h1>\n"
;;                (www-format-encode-string (est-format-object object))))
;;       (princ "<form action=\"set.cgi\" method=\"GET\">\n")
;;       (princ
;;        (encode-coding-string
;;         (format "<p>(%s : <input type=\"text\" name=\"%s\"
;; size=\"30\" maxlength=\"30\" value=\"%s\">)</p>
;; "
;;                 genre genre
;;                 (decode-uri-string uri-object 'utf-8-mcs-er))
;;         'utf-8-mcs-er))
;;       (setq object-spec
;;             (if (eq genre 'character)
;;                 (char-attribute-alist object)
;;               (concord-object-spec object)))
;;       (if (string-match "\\*" (setq str (symbol-name feature-name)))
;;           (setq base-name (intern (substring str 0 (match-beginning 0)))
;;                 metadata-name (intern (substring str (match-end 0))))
;;         (setq base-name feature-name))
;;       (unless (assq base-name object-spec)
;;         (setq object-spec (cons (cons base-name nil)
;;                               object-spec)))
;;       (dolist (cell (sort object-spec
;;                           (lambda (a b)
;;                             (char-attribute-name< (car a)(car b)))))
;;         (cond
;;          ((eq (car cell) feature-name)
;;           (www-edit-display-feature-input-box object feature-name format)
;;           )
;;          (t
;;           (princ "<p>")
;;           (princ
;;            (www-format-eval-list
;;             (or (char-feature-property (car cell) 'format)
;;                 '((name) " : " (value)))
;;             object (car cell) lang uri-object))
;;           (princ "</p>\n")
;;           (when (and (eq base-name (car cell)) metadata-name)
;;             (princ "<ul>\n")
;;             (princ "<li>")
;;             (www-edit-display-feature-input-box object feature-name format)
;;             (princ "</li>")
;;             (princ "</ul>"))
;;           ))
;;         )
;;       (princ "</form>\n")
;;       )))

(defun www-edit-display-feature-desc (uri-feature-name
				      uri-property-name
				      &optional lang
				      object-genre uri-object)
  (let ((feature-name (www-uri-decode-feature-name uri-feature-name))
	(property-name (www-uri-decode-feature-name uri-property-name))
	name@lang)
    (princ
     (encode-coding-string
      (format "<head>
<title>CHISE-wiki feature: %s</title>
</head>\n"
	      feature-name)
      'utf-8-mcs-er))
    (princ "<body>\n")
    (princ "<form action=\"set.cgi\" method=\"GET\">\n")
    (princ
     (encode-coding-string
      (format "<h1>feature : <input type=\"text\" name=\"feature\"
size=\"30\" maxlength=\"30\" value=\"%s\"></h1>\n"
	      feature-name)
      'utf-8-mcs-er))
    (princ
     (encode-coding-string
      (format "<p>(<input type=\"text\" name=\"char\"
size=\"30\" maxlength=\"30\" value=\"%s\">に限\u5B9Aしない)
"
	      (decode-uri-string uri-object 'utf-8-mcs-er))
      'utf-8-mcs-er))
    (princ "<p>")
    (if (eq property-name 'name)
	;; (www-edit-display-input-box
        ;;  feature-name
        ;;  property-name
        ;;  (or (www-format-feature-name* feature-name) ""))
	(www-edit-display-feature-input-box
	 feature-name property-name
	 'string (or (www-format-feature-name* feature-name) ""))
      (www-html-display-paragraph
       (format "name : %s [[[edit|edit.cgi?feature=%s&property=name]]]"
	       (or (www-format-feature-name* feature-name) "")
	       ;; (char-feature-property feature-name 'name)
	       uri-feature-name ; (www-uri-encode-feature-name feature-name)
	       )))
    (when lang
      (setq name@lang (intern (format "name@%s" lang)))
      (if (eq property-name name@lang)
          ;; (www-edit-display-input-box
          ;;  feature-name
          ;;  name@lang
          ;;  (or (char-feature-property feature-name name@lang) ""))
	  (www-edit-display-feature-input-box
	   feature-name name@lang
	   'string (or (char-feature-property feature-name name@lang) ""))
	(www-html-display-paragraph
	 (format "%s : %s [[[edit|edit.cgi?feature=%s&property=%s]]]"
		 name@lang
		 (or (char-feature-property feature-name name@lang) "")
		 uri-feature-name
		 name@lang))))
    (www-html-display-paragraph
     (format "type : %s"
             (or (www-feature-type feature-name)
		 ;; (char-feature-property feature-name 'type)
		 'generic)))
    (if (eq property-name 'value-format)
	(www-edit-display-feature-input-box
	 feature-name property-name
	 'default ; 'wiki-text
	 (or (www-feature-value-format feature-name)
	     'default))
      (www-html-display-paragraph
       (format "value-format : %s [[[edit|edit.cgi?feature=%s&property=value-format]]]"
	       (www-xml-format-list
		(or (www-feature-value-format feature-name)
		    'default))
	       uri-feature-name)))
    (if (eq property-name 'value-presentation-format)
	(www-edit-display-feature-input-box
	 feature-name property-name
	 'default ; 'wiki-text
	 (or (www-feature-value-format feature-name)
	     'default))
      (www-html-display-paragraph
       (format "value-presentation-format : %s [[[edit|edit.cgi?feature=%s&property=value-presentation-format]]]"
	       (www-xml-format-list
		(or (www-feature-value-format feature-name)
		    'default))
	       uri-feature-name)))
    (if (eq property-name 'format)
	(www-edit-display-feature-input-box
	 feature-name property-name
	 'wiki-text
	 (or (char-feature-property feature-name 'format)
	     '((name) " : " (value))))
      (www-html-display-paragraph
       (format "format : %s [[[edit|edit.cgi?feature=%s&property=format]]]"
	       (www-xml-format-list
		(char-feature-property feature-name 'format))
	       uri-feature-name)))
    (www-html-display-paragraph
     (format "description : %s"
             (or (char-feature-property feature-name 'description)
		 "")))
    (when lang
      (www-html-display-paragraph
       (format "description@%s : %s"
	       lang
	       (or (char-feature-property
		    feature-name
		    (intern (format "description@%s" lang)))
		   ""))))
    (princ "</form>\n")
    ))
  
(defun www-batch-edit ()
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((target (pop command-line-args-left))
	     (user (pop command-line-args-left))
	     (accept-language (pop command-line-args-left))
	     (lang
	      (intern (car (split-string
			    (car (split-string
				  (car (split-string accept-language ","))
				  ";"))
			    "-"))))
	     ret)
	(princ "Content-Type: text/html; charset=UTF-8

<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
            \"http://www.w3.org/TR/html4/loose.dtd\">
<html lang=\"ja\">
")
	(setq target
	      (mapcar (lambda (cell)
			(if (string-match "=" cell)
			    (cons
			     (intern
			      (decode-uri-string
			       (substring cell 0 (match-beginning 0))
			       'utf-8-mcs-er))
			     (substring cell (match-end 0)))
			  (list (decode-uri-string cell 'utf-8-mcs-er))))
		      (split-string target "&")))
	(setq ret (car target))
	(cond ((eq (car ret) 'char)
	       (www-edit-display-object-desc
		'character
                (cdr ret) ; (decode-uri-string (cdr ret) 'utf-8-mcs-er)
		(decode-uri-string (cdr (assq 'feature target))
				   'utf-8-mcs-er)
		lang
		(decode-uri-string (cdr (assq 'format target))
				   'utf-8-mcs-er))
	       )
	      ((eq (car ret) 'feature)
	       (www-edit-display-feature-desc
		(decode-uri-string (cdr ret) 'utf-8-mcs-er)
		(decode-uri-string (cdr (assq 'property target))
				   'utf-8-mcs-er)
		lang
		(car (nth 3 target))
                (cdr (nth 3 target))
                ;; (cdr (assq 'char target))
                )
	       )
	      (t
	       (www-edit-display-object-desc
                (car ret)
		(cdr ret)
		(decode-uri-string (cdr (assq 'feature target))
				   'utf-8-mcs-er)
		lang
		(decode-uri-string (cdr (assq 'format target))
				   'utf-8-mcs-er))
	       ))
	(www-html-display-paragraph
	 (format "%S" target))
	(princ "\n<hr>\n")
	(princ (format "user=%s\n" user))
	(princ (format "local user=%s\n" (user-login-name)))
	(princ (format "lang=%S\n" lang))
	(princ emacs-version)
	(princ " CHISE ")
	(princ (encode-coding-string xemacs-chise-version 'utf-8-jp-er))
	(princ "
</body>
</html>")
	)
    (error nil
	   (princ (format "%S" err)))
    ))

(provide 'cwiki-edit)
