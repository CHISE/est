;; -*- coding: utf-8-mcs-er -*-
(defvar chise-wiki-view-url "view.cgi")
(defvar chise-wiki-edit-url "edit.cgi")

(require 'cwiki-edit)

;; (defun www-add-display-feature-input-box (char &optional format)
;;   (unless format
;;     (setq format 'default))
;;   (princ
;;    "<p><input type=\"text\" name=\"feature-name\"
;; size=\"32\" maxlength=\"256\" value=\"\">")
;;   (princ (encode-coding-string " \u2190 " 'utf-8-mcs-er))
;;   (princ
;;    (format "%s<input type=\"text\" name=\"%s\"
;; size=\"64\" maxlength=\"256\" value=\"\">
;; <input type=\"submit\" value=\"set\" /></p>
;; "
;;            (if (or (eq format 'HEX)(eq format 'hex))
;;                "0x"
;;              "")
;;            format))
;;   )

(defun www-add-display-object-desc (genre uri-object &optional lang format)
  (www-edit-display-object-desc genre uri-object "" lang format)
  )

(defun www-batch-add ()
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((target (pop command-line-args-left))
	     (user (pop command-line-args-left))
	     (accept-language (pop command-line-args-left))
	     (lang
	      (intern (car (split-string
			    (car (split-string
				  (car (split-string accept-language ","))
				  ";"))
			    "-"))))
	     ret)
	(princ "Content-Type: text/html; charset=UTF-8

<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
            \"http://www.w3.org/TR/html4/loose.dtd\">
<html lang=\"ja\">
")
	(setq target
	      (mapcar (lambda (cell)
			(if (string-match "=" cell)
			    (cons
			     (intern
			      (decode-uri-string
			       (substring cell 0 (match-beginning 0))
			       'utf-8-mcs-er))
			     (substring cell (match-end 0)))
			  (list (decode-uri-string cell 'utf-8-mcs-er))))
		      (split-string target "&")))
	(setq ret (car target))
	(cond (t ; (eq (car ret) 'char)
	       (www-add-display-object-desc
                (car ret)
		(cdr ret)
		lang
		(decode-uri-string (cdr (assq 'format target))
				   'utf-8-mcs-er))
	       )
              ;; ((eq (car ret) 'feature)
              ;;  (www-add-display-feature-desc
              ;;   (decode-uri-string (cdr ret) 'utf-8-mcs-er)
              ;;   (decode-uri-string (cdr (assq 'property target))
              ;;                      'utf-8-mcs-er)
              ;;   lang
              ;;   (cdr (assq 'char target))
              ;;   ;; (decode-uri-string (cdr (assq 'char target))
              ;;   ;;                    'utf-8-mcs-er)
              ;;   ))
	      )
	(www-html-display-paragraph
	 (format "%S" target))
	(princ "\n<hr>\n")
	(princ (format "user=%s\n" user))
	(princ (format "local user=%s\n" (user-login-name)))
	(princ (format "lang=%S\n" lang))
	(princ emacs-version)
	(princ " CHISE ")
	(princ (encode-coding-string xemacs-chise-version 'utf-8-jp-er))
	(princ "
</body>
</html>")
	)
    (error nil
	   (princ (format "%S" err)))
    ))
