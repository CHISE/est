;; -*- coding: utf-8-mcs-er -*-
(require 'cwiki-common)
(require 'est-eval)
(require 'est-format)
(require 'est-xml)

(defun www-format-value (object feature-name
				&optional value format
				without-tags without-edit)
  (if (symbolp object)
      (setq object (concord-decode-object '=id object 'feature)))
  (unless value
    (setq value (www-get-feature-value object feature-name)))
  (www-format-apply-value object feature-name
			  format nil value nil nil
			  without-tags without-edit)
  )

(defun www-format-value-as-char-list (value &optional without-tags)
  (est-format-unit
   (est-eval-value-as-object-list value " ") without-tags))

(defun www-format-apply-value (object feature-name
				      format props value
				      &optional uri-object uri-feature
				      without-tags without-edit)
  (est-format-unit
   (est-eval-apply-value object feature-name format props value uri-object)
   without-tags without-edit))

(defun www-format-eval-list (format-list object feature-name
					 &optional lang uri-object
					 without-tags without-edit)
  (est-format-unit
   (est-eval-list format-list object feature-name lang uri-object)
   without-tags without-edit))


;;; @ End.
;;;

(provide 'cwiki-format)

;;; cwiki-format.el ends here
